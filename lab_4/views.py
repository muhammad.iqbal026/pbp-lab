from django.shortcuts import render, redirect
from .models import Note
from .forms import NoteForm


def index(request):
	note = Note.objects.all()
	print(note)
	response = {'note': note}
	return render(request, 'lab4_index.html', response)


def add_note(request):
	if request.method == 'POST':
		form = NoteForm(request.POST or None)
		print(form)
		if form.is_valid():
			form.save()

		return redirect('/lab-4/')
	else:
		return render(request, 'lab4_form.html')


def note_list(request):
	note = Note.objects.all()
	response = {'note': note}
	return render(request, 'lab4_note_list.html', response)