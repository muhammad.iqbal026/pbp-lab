from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from .models import Friend
from .forms import FriendForm


@login_required(login_url='/admin/login/')
def index(request):
	friends = Friend.objects.all()
	print(friends)
	response = {'friends': friends}
	return render(request, 'lab3_index.html', response)


@login_required(login_url='/admin/login/')
def add_friend(request):
	if request.method == 'POST':
		form = FriendForm(request.POST or None)
		print(form)
		if form.is_valid():
			form.save()

		return redirect('/lab-3/')
	else:
		return render(request, 'lab3_form.html')