import 'package:flutter/material.dart';
import 'package:footer/footer_view.dart';
import 'package:footer/footer.dart';
import './form_booking.dart';

class Booking extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('CovidConsult'),
        actions: [
          PopupMenuButton<int>(
              itemBuilder: (context) => [
                const PopupMenuItem<int>(
                  value: 0,
                  child: Text('Consultation'),
                ),
                const PopupMenuItem<int>(
                  value: 1,
                  child: Text('Forum'),
                ),
                const PopupMenuItem<int>(
                  value: 2,
                  child: Text('ObatPedia'),
                ),
                const PopupMenuItem<int>(
                  value: 3,
                  child: Text('Article'),
                ),
              ])
        ],
      ),
      body: FooterView(
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Align(
                alignment: Alignment.center,
                child: Padding(
                  padding: EdgeInsetsDirectional.only(top: 25.0, bottom: 10.0, start: 16.0, end: 16.0),
                  child: Text(
                    "Consultation",
                    softWrap: true,
                    style: TextStyle(
                      fontSize: 40,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),

              Align(
                alignment: Alignment.center,
                child: Padding(
                  padding: EdgeInsetsDirectional.only(top: 25.0, bottom: 20.0, start: 16.0, end: 16.0),
                  child: Text(
                    "We provide to you the best choiches for you. "
                        "Adjust it to your health needs and make sure your "
                        "undergo treatment with our highly qualified doctors "
                        "you can consult with us which type of service is "
                        "suitable for your health",
                    softWrap: true,
                    style: TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),

              Align(
                alignment: Alignment.center,
                child: MaterialButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  color: Colors.deepPurple,
                  onPressed: () {
                    navigateToBookingForm(context);
                  },
                  child: Text(
                    "Book Consultation",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      fontSize: 17.0,
                    ),
                  ),
                ),
              ),

              Card(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    ListTile(
                      title: Text("Senin"),
                    ),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          child: Text("Waktu"),
                        ),

                        Container(
                          child: Text("Dokter"),
                        ),
                      ],
                    ),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          child: Text("15.00 - 19.00"),
                        ),

                        Container(
                          child: Text("Dokter Z"),
                        ),
                      ],
                    )
                  ],
                ),
              ),

              Card(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    ListTile(
                      title: Text("Selasa"),
                    ),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          child: Text("Waktu"),
                        ),
                        Container(
                          child: Text("Dokter"),
                        ),
                      ],
                    ),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          child: Text("10.00 - 13.00"),
                        ),
                        Container(
                          child: Text("Dokter Q"),
                        ),
                      ],
                    )
                  ],
                ),
              ),

              Card(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    ListTile(
                      title: Text("Rabu"),
                    ),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          child: Text("Waktu"),
                        ),
                        Container(
                          child: Text("Dokter"),
                        ),
                      ],
                    ),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          child: Text("12.00 - 18.00"),
                        ),
                        Container(
                          child: Text("Dokter AA"),
                        ),
                      ],
                    )
                  ],
                ),
              ),

              Card(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    ListTile(
                      title: Text("Kamis"),
                    ),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          child: Text("Waktu"),
                        ),
                        Container(
                          child: Text("Dokter"),
                        ),
                      ],
                    ),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          child: Text("09.00 - 15.00"),
                        ),
                        Container(
                          child: Text("Dokter C"),
                        ),
                      ],
                    )
                  ],
                ),
              ),

              Card(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    ListTile(
                      title: Text("Jumat"),
                    ),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          child: Text("Waktu"),
                        ),
                        Container(
                          child: Text("Dokter"),
                        ),
                      ],
                    ),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          child: Text("10.00 - 13.00"),
                        ),
                        Container(
                          child: Text("Dokter B"),
                        ),
                      ],
                    )
                  ],
                ),
              )
            ],
          )
        ],

        footer: Footer(
            backgroundColor: Colors.purple,
            child: Padding(
              padding: new EdgeInsets.all(10.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Covid Consult"),
                  Image.asset(
                    'assets/image/gitlabLogo.png',
                    width: 150,
                    height: 75,
                  ),
                  Text("Made by: C01-2021"),
                ],
              ),
            )
        ),

        flex: 1,
      ),
    );
  }

  void navigateToBookingForm(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) => FormBooking()));
  }
}