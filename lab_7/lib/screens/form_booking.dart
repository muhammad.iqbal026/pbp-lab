import 'package:flutter/material.dart';
import './booking.dart';

class FormBooking extends StatelessWidget {
  static const routeName = '/form';
  final title = 'Booking Form';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: const Text('CovidConsult'),
        actions: [
          PopupMenuButton<int>(
              itemBuilder: (context) => [
                const PopupMenuItem<int>(
                  value: 0,
                  child: Text('Consultation'),
                ),
                const PopupMenuItem<int>(
                  value: 1,
                  child: Text('Forum'),
                ),
                const PopupMenuItem<int>(
                  value: 2,
                  child: Text('ObatPedia'),
                ),
                const PopupMenuItem<int>(
                  value: 3,
                  child: Text('Article'),
                ),
              ])
        ],
      ),

      body: SingleChildScrollView(
        child: Column(
          children: <Widget> [
            Center(
              child: Padding(
                padding: EdgeInsets.all(35.0),
                child: Text(
                  "Booking Form",
                  style: TextStyle(
                    fontSize: 35.0,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              )
            ),

            Center(
                child: Padding(
                  padding: EdgeInsets.all(35.0),
                  child: Text(
                    "---------------------------------------------------------",
                    style: TextStyle(
                      fontSize: 10.0,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                )
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: EdgeInsetsDirectional.only(top: 25.0, bottom: 10.0, start: 16.0, end: 16.0),
                child: Text(
                  "Nama Dokter",
                  softWrap: true,
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ),
            ),

            Center(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 16.0),
                child: Column(
                  children: <Widget> [
                    TextField(
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        hintText: "Masukkan Nama Dokter",
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(width: 1, color: Colors.purple),
                          borderRadius: BorderRadius.circular(15),
                        )
                      ),
                    )
                  ],
                ),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: EdgeInsetsDirectional.only(top: 25.0, bottom: 10.0, start: 16.0, end: 16.0),
                child: Text(
                  "Waktu Konsultasi",
                  softWrap: true,
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ),
            ),

            Center(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 16.0),
                child: Column(
                  children: <Widget> [
                    TextField(
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          hintText: "Masukkan Waktu Konsultasi",
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(width: 1, color: Colors.purple),
                            borderRadius: BorderRadius.circular(15),
                          )
                      ),
                    )
                  ],
                ),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: EdgeInsetsDirectional.only(top: 25.0, bottom: 10.0, start: 16.0, end: 16.0),
                child: Text(
                  "Hari Konsultasi",
                  softWrap: true,
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ),
            ),

            Center(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 16.0),
                child: Column(
                  children: <Widget> [
                    TextField(
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          hintText: "Masukkan Hari Konsultasi",
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(width: 1, color: Colors.purple),
                            borderRadius: BorderRadius.circular(15),
                          )
                      ),
                    )
                  ],
                ),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: EdgeInsetsDirectional.only(top: 25.0, bottom: 10.0, start: 16.0, end: 16.0),
                child: Text(
                  "Nama Pasien",
                  softWrap: true,
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ),
            ),

            Center(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 16.0),
                child: Column(
                  children: <Widget> [
                    TextField(
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          hintText: "Masukkan Nama Pasien",
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(width: 1, color: Colors.purple),
                            borderRadius: BorderRadius.circular(15),
                          )
                      ),
                    )
                  ],
                ),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: EdgeInsetsDirectional.only(top: 25.0, bottom: 10.0, start: 16.0, end: 16.0),
                child: Text(
                  "Nomor Handphone",
                  softWrap: true,
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ),
            ),

            Center(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 16.0),
                child: Column(
                  children: <Widget> [
                    TextField(
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          hintText: "Masukkan Nomor Handphone",
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(width: 1, color: Colors.purple),
                            borderRadius: BorderRadius.circular(15),
                          )
                      ),
                    )
                  ],
                ),
              ),
            ),

            Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: EdgeInsetsDirectional.only(top: 25.0, bottom: 10.0, start: 16.0, end: 16.0),
                child: Text(
                  "Email",
                  softWrap: true,
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ),
            ),

            Center(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 16.0),
                child: Column(
                  children: <Widget> [
                    TextField(
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          hintText: "Masukkan Email",
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(width: 1, color: Colors.purple),
                            borderRadius: BorderRadius.circular(15),
                          )
                      ),
                    )
                  ],
                ),
              ),
            ),

            Center(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 16.0),
                child: Column(
                  children: <Widget> [
                    Center(
                      child: Row(
                        children: [
                          MaterialButton(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            color: Colors.deepPurple,
                            onPressed: () {
                              navigateToBooking(context);
                            },
                            child: Text(
                              "Book Consultation",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
                                fontSize: 17.0,
                              ),
                            ),
                          ),

                          MaterialButton(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            color: Colors.purple,
                            onPressed: () {
                              navigateToBooking(context);
                            },
                            child: Text(
                              "Cancel",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
                                fontSize: 17.0,
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      )
    );
  }

  void navigateToBooking(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) => Booking()));
  }
}