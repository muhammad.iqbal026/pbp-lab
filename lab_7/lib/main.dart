import 'package:flutter/material.dart';
import 'package:lab_7/screens/form_booking.dart';
import './screens/booking.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'CovidConsult',
      theme: ThemeData(
        brightness: Brightness.dark,
        scaffoldBackgroundColor: Colors.grey[850],

      ),
      initialRoute: '/', // Default is '/'
      routes: {
        '/': (context) => Booking(),
        FormBooking.routeName: (context) => FormBooking(),
      },
    );
  }
}