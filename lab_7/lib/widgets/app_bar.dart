import 'package:flutter/material.dart';

class BaseAppBar extends StatelessWidget {
  final String title;

  const BaseAppBar(this.title);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(title),
      actions: [
        PopupMenuButton<int>(
            itemBuilder: (context) => [
              const PopupMenuItem<int>(
                value: 0,
                child: Text('Consultation'),
              ),
              const PopupMenuItem<int>(
                value: 1,
                child: Text('Forum'),
              ),
              const PopupMenuItem<int>(
                value: 2,
                child: Text('ObatPedia'),
              ),
              const PopupMenuItem<int>(
                value: 3,
                child: Text('Article'),
              ),
            ])
      ],
    );
  }
}