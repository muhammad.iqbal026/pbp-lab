import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:footer/footer.dart';
import 'package:footer/footer_view.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:material_floating_search_bar/material_floating_search_bar.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'CovidConsult',
      theme: ThemeData(
        brightness: Brightness.dark,
        scaffoldBackgroundColor: Colors.grey[850],

      ),
      home: Scaffold(
        appBar: AppBar(
          title: const Text('CovidConsult'),
          actions: [
            PopupMenuButton<int>(
                itemBuilder: (context) => [
                  const PopupMenuItem<int>(
                    value: 0,
                    child: Text('Consultation'),
                  ),
                  const PopupMenuItem<int>(
                    value: 1,
                    child: Text('Forum'),
                  ),
                  const PopupMenuItem<int>(
                    value: 2,
                    child: Text('ObatPedia'),
                  ),
                  const PopupMenuItem<int>(
                    value: 3,
                    child: Text('Article'),
                  ),
                ])
          ],
        ),
        body: FooterView(
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    const Text("Consultation"),
                    const Text("We provide to you the best choiches for you. "
                        "Adjust it to your health needs and make sure your "
                        "undergo treatment with our highly qualified doctors "
                        "you can consult with us which type of service is "
                        "suitable for your health"),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.purple,
                        onPrimary: Colors.white,
                      ),
                      onPressed: () {},
                      child: Text("Book Consultation"),
                    ),
                  ],
                ),

                Card(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      ListTile(
                        title: Text("Senin"),
                      ),

                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Container(
                            child: Text("Waktu"),
                          ),

                          Container(
                            child: Text("Dokter"),
                          ),
                        ],
                      ),

                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Container(
                            child: Text("15.00 - 19.00"),
                          ),

                          Container(
                            child: Text("Dokter Z"),
                          ),
                        ],
                      )
                    ],
                  ),
                ),

                Card(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      ListTile(
                        title: Text("Selasa"),
                      ),

                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Container(
                            child: Text("Waktu"),
                          ),
                          Container(
                            child: Text("Dokter"),
                          ),
                        ],
                      ),

                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Container(
                            child: Text("10.00 - 13.00"),
                          ),
                          Container(
                            child: Text("Dokter Q"),
                          ),
                        ],
                      )
                    ],
                  ),
                ),

                Card(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      ListTile(
                        title: Text("Rabu"),
                      ),

                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Container(
                            child: Text("Waktu"),
                          ),
                          Container(
                            child: Text("Dokter"),
                          ),
                        ],
                      ),

                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Container(
                            child: Text("12.00 - 18.00"),
                          ),
                          Container(
                            child: Text("Dokter AA"),
                          ),
                        ],
                      )
                    ],
                  ),
                ),

                Card(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      ListTile(
                        title: Text("Kamis"),
                      ),

                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Container(
                            child: Text("Waktu"),
                          ),
                          Container(
                            child: Text("Dokter"),
                          ),
                        ],
                      ),

                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Container(
                            child: Text("09.00 - 15.00"),
                          ),
                          Container(
                            child: Text("Dokter C"),
                          ),
                        ],
                      )
                    ],
                  ),
                ),

                Card(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      ListTile(
                        title: Text("Jumat"),
                      ),

                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Container(
                            child: Text("Waktu"),
                          ),
                          Container(
                            child: Text("Dokter"),
                          ),
                        ],
                      ),

                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Container(
                            child: Text("10.00 - 13.00"),
                          ),
                          Container(
                            child: Text("Dokter B"),
                          ),
                        ],
                      )
                    ],
                  ),
                )
              ],
            )
          ],

          footer: Footer(
            backgroundColor: Colors.purple,
            child: Padding(
              padding: new EdgeInsets.all(10.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Covid Consult"),
                  Image.asset(
                    'assets/image/gitlabLogo.png',
                    width: 150,
                    height: 75,
                  ),
                  Text("Made by: C01-2021"),
                ],
              ),
            )
          ),

          flex: 1,
        ),
      )
    );
  }
}
