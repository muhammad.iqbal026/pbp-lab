from django.shortcuts import render
from .models import Note


def index(request):
    note = Note.objects.all()
    response = {'note': note}
    return render(request, 'lab5_index.html', response)