JSON atau JavaScript Object Notation adalah format yang menggunakan human-readable text untuk menyimpan dan mentransfer data dalam pasangan key dan value

XML atau Extensive Markup Language adalah markup language yang mendefinisikan seperangkat aturan untuk meng-encode dokumen dalam format yang human-readable dan machine-readable

HTML atau Hyper Text Markup Language adalah markup language standar yang dirancang untuk menampilkan dokumen di web browser

Nomor 1
JSON
- objek memiliki type
- type: string, number, array, boolean
- data bisa langsung diakses dari objek JSON
- di-support oleh mayoritas web browser
- tidak memiliki kemampuan menampilkan data
- hanya mendukung type berupa text dan angka
- value mudah diambil
- di-support oleh banyak Ajax toolkit
- cara otomatis untuk de/serializing JavaScript
- objek memiliki native support
- hanya support UTF-8 encoding
- tidak support comments
- mudah dibaca
- tidak support namespaces
- kurang aman

XML
- data tidak memiliki tipe
- seluruh data berupa string
- data harus di-parse baru bisa diakses
- cross-browser XML agak rumit
- mampu menampilkan data karena merupakan markup language
- support bermacam data type seperti angka, text, gambar, chart, graph, dll. Juga bisa mentransfer struktur atau format dari data aslinya
- value susah diambil
- tidak disupport penuh oleh Ajax toolkit
- perlu JavaScript kode untuk de/serialize XML
- objek diekspresikan sesuai kebutuhan menggunakan attributes dan elements
- support berbagai macam tipe encoding
- support comments
- lebih susah dibaca dan dimengerti
- support namespaces
- lebih aman dari JSON

Nomor 2
HTML
- markup language
- tidak case sensitive
- dapat digunakan sebaga presentation language
- tag sudah didefinisikan
- closing tag tidak wajib
- white space tidak dipertahankan
- menampilkan design web page sesuai apa yang ditampilkan di client-side
- digunakan untuk menampilkan data
- static
- memiliki native support
- memahami null value
- tidak memerlukan kode aplikasi tambahan untuk mem-parse text

XML
- standard markup language yang mendefinisikan markup language lain
- case sensitive
- bukan presentation language ataupun bahasa pemrograman
- tag flexibel didefinisikan sesuai kebutuhan programmer
- biasanya closing tag wajib
- dapat mempertahankan white spaces
- transportasi data dari database dan aplikasi terkait
- digunakan untuk mentransfer data
- dynamic
- objek diekspresikan dengan bantuan elements dan attributes sesuai kebutuhan
- membutuhkan xsi:nil untuk mengenali null value
- membutuhkan XML DOM untuk merubah text menjadi JavaScript objek

Sumber: 
https://www.guru99.com/json-vs-xml-difference.html
https://www.upgrad.com/blog/html-vs-xml/